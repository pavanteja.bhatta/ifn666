import React from 'react';
import { AgGridReact } from 'ag-grid-react';
import { Clear } from '@material-ui/icons';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import { Input, InputLabel, MenuItem, FormControl, Select, Button, TextField } from '@material-ui/core';
import "./grid.css";
import HistoryComponent from "../history/history";
import { getIndustry } from "../../services/industry.service";
// import ListItemText from '@material-ui/core/ListItemText';
// import Checkbox from '@material-ui/core/Checkbox';
// import Chip from '@material-ui/core/Chip';
// import { useIndustry } from "../../services/effects/industry.effects";
// import LoadingComponent from "../loading/loading";
// import { Alert } from "reactstrap"

class GridComponent extends React.Component {

	constructor(props) {
		super(props);
		this.searchedData = this.props.data;
		this.state = {
			columnDefs: [{
				headerName: "Symbol", field: "symbol", sortable: true, filter: true
			}, {
				headerName: "Name", field: "name", sortable: true,
			}, {
				headerName: "Industry", field: "industry", sortable: true,
			}],
			rowData: this.props.data,
			searchText: "",
			theme: createMuiTheme({
				palette: {
					primary: green,
				},
			}),
			industries: ["Health Care", "Industrials", "Consumer Discretionary", "Information Technology", "Consumer Staples",
				"Utilities", "Financials", "Real Estate", "Materials", "Energy", "Telecommunication Services"],
			industrySearch: "",
			industryItem: undefined
		};
	}

	onGridCellClicked() {
		let selectedRow = this.gridApi.getSelectedRows()[0];
		this.setState({ industryItem: selectedRow.symbol });
	}

	onGridReady = params => {
		this.gridApi = params.api;
		params.api.setRowData(this.searchedData);
	}

	callRefresh(params, gridApi) {
		gridApi.setRowData(params);
	}

	render() {
		if (this.state.industryItem) {
			return (
				<div>
					<HistoryComponent symbol={this.state.industryItem}></HistoryComponent>
				</div>
			)
		}
		return (
			<div>
				<h3 className="display-5 left">Filters:</h3>
				<br />
				<div className="row">
					<div className="col-sm-6">
						<TextField className="large-input" id="outlined-search" label="Type to search by symbol" type="search" variant="outlined" onChange={event => this.changeSearch(event.target.value)} />
					</div>
					<div className="col-sm-6">
						<FormControl className="large-input">
							<InputLabel id="demo-mutiple-name-label">Industry Search</InputLabel>
							<Select
								labelId="demo-mutiple-name-label"
								variant="outlined"
								id="demo-mutiple-name"
								onChange={(event) => this.handleIndustryChange(event.target.value)}
								input={<Input value={this.state.industrySearch} />}>
								{this.state.industries.map((industry) => (
									<MenuItem key={industry} value={industry}>
										{industry}
									</MenuItem>
								))}
							</Select>
						</FormControl>
						<ThemeProvider theme={this.state.theme}>
							<Button variant="outlined" className="large" color="primary" onClick={(event) => this.clearIndustry()} startIcon={<Clear />}>
								Clear
        					</Button>
						</ThemeProvider>
					</div>
				</div>
				<br />
				<div className="ag-theme-alpine flex">
					<AgGridReact
						enableCellChangeFlash={true}
						onGridReady={this.onGridReady}
						columnDefs={this.state.columnDefs}
						rowSelection="single"
						pagination="true"
						paginationAutoPageSize="true"
						colWidth="300"
						onCellClicked={this.onGridCellClicked.bind(this)}>
					</AgGridReact>
				</div>
			</div>
		);
	}

	changeSearch(value) {
		this.setState({ searchText: value });
		let industryText = this.state.industrySearch;
		let rowItems = [];
		if (!value && !industryText) {
			this.searchedData = this.state.rowData;
		} else {
			for (let i = 0; i < this.state.rowData.length; i++) {
				if (industryText) {
					if (this.state.rowData[i].symbol.toLowerCase().includes(value) && this.state.rowData[i].industry.includes(industryText)) {
						rowItems.push(this.state.rowData[i]);
					}
				} else {
					if (this.state.rowData[i].symbol.toLowerCase().includes(value)) {
						rowItems.push(this.state.rowData[i]);
					}
				}
				this.searchedData = rowItems;
			}
		}
		this.callRefresh(this.searchedData, this.gridApi);
	}

	handleIndustryChange = async (value) => {
		this.setState({ industrySearch: value });
		let stocks = await getIndustry(value);
		let searchText = this.state.searchText;
		let filteredStocks = [];
		if (searchText) {
			stocks.forEach(stock => {
				if (stock.symbol.toLowerCase().includes(searchText)) {
					filteredStocks.push(stock);
				}
			});
			this.callRefresh(filteredStocks, this.gridApi);
		} else {
			this.callRefresh(stocks, this.gridApi);
		}
		// let rowItems = [];
		// this.setState({ industrySearch: value });
		// let searchText = this.state.searchText;
		// if (!value && !searchText) {
		// 	this.setState({ searchedData: this.state.rowData });
		// } else {
		// 	for (let i = 0; i < this.state.rowData.length; i++) {
		// 		if (searchText) {
		// 			if (this.state.rowData[i].symbol.toLowerCase().includes(searchText) && this.state.rowData[i].industry.includes(value)) {
		// 				rowItems.push(this.state.rowData[i]);
		// 			}
		// 		} else {
		// 			if (this.state.rowData[i].industry.includes(value)) {
		// 				rowItems.push(this.state.rowData[i]);
		// 			}
		// 		}
		// 	}
		// 	this.setState({ searchedData: rowItems });
		// }
	}

	clearIndustry = async() => {
		await this.setState({ industrySearch: "" });
		this.changeSearch(this.state.searchText);
	}
}

export default GridComponent