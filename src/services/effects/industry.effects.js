import { useState, useEffect } from "react";
import { getIndustry } from "../industry.service";

function useIndustry(industry = undefined) {
	const [industryLoading, setLoading] = useState(true);
	const [industryStocks, setStocks] = useState([]);
	const [industryError, setError] = useState(null);

	useEffect(() => {
		getIndustry(industry)
			.then(stocks => {
				if (stocks.error || stocks.error == "true") {
					throw stocks.error;
				}
				setStocks(stocks);
				setLoading(false);
			})
			.catch(e => {
				setError(e);
				setLoading(false);
			});
	}, [industry]);

	return {
		industryLoading,
		industryStocks,
		industryError
	};
}

export { useIndustry };