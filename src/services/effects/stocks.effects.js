import { useState, useEffect } from "react";
import { getStocks } from "../stocks.service";

function useStocks(symbol = undefined) {
	const [stocksLoading, setLoading] = useState(true);
	const [stocks, setStocks] = useState([]);
	const [stocksError, setError] = useState(null);

	useEffect((symbol) => {
		getStocks(symbol)
			.then(stocks => {
				setStocks(stocks);
				setLoading(false);
			})
			.catch(e => {
				setError(e);
				setLoading(false);
			});
	}, [symbol]);

	return {
		stocksLoading,
		stocks,
		stocksError
	};
}

export { useStocks };