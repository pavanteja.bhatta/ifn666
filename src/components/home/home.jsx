import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';
import { TrendingUp, Info } from '@material-ui/icons';
import "./home.css";

class HomeComponent extends React.Component {
	constructor(props) {
		super(props);
		this.state = { ...props };
	}

	render() {
		return (
			<div>
				<h1 className="display-3"><TrendingUp style={{ fontSize: "4.5rem" }}/>&nbsp;Stock Prices</h1>
				<p className="lead">Welcome to the Stock Market Page. You may click on stocks to view all the stocks or search to view the latest 100 days of activity.</p>
				<p className="lead">
					<Link to="/stocks"><Button variant="outlined" color="secondary" startIcon={<TrendingUp />}>Stocks</Button></Link>
				</p>
				<hr className="my-2" />
				<p>To know more about the project, please click on the button below.</p>
				<p className="lead">
					<Link to="/about"><Button variant="outlined" color="primary" startIcon={<Info />}>About</Button></Link>
				</p>
			</div>
		);
	}
}

export default HomeComponent;