import config from "../config";

function getIndustry(industry = undefined) {
	let api = config.apiEndPoint + "/industry";
	if (industry) {
		api += "?industry=" + industry;
	}

	return fetch(api)
		.then(res => res.json())
		.then(res => {
			if (res.error || res.error === "true") {
				return res;
			} else {
				return res.map(stock => ({
					symbol: stock.symbol,
					name: stock.name,
					industry: stock.industry,
				}));
			}
		});
}

export { getIndustry };