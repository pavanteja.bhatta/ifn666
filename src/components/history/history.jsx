import React from 'react';
import { useHistory } from '../../services/effects/history.effects';
import HistoryGridComponent from '../history-grid/history-grid';
import LoadingComponent from "../loading/loading";
import { Alert } from "reactstrap"

export default function HistoryComponent(props) {
	let symbol = props.symbol;
	const { historyLoading, history, historyError } = useHistory(props.symbol, props.date);

	if (historyLoading) {
		return (
			<LoadingComponent>
			</LoadingComponent>
		);
	} else if (historyError) {
		return (
			<Alert color="danger">
				{getErrorMessage(historyError)}
			</Alert>
		);
	} else {
		return (
			<HistoryGridComponent data={history} symbol={symbol} date={props.date}></HistoryGridComponent>
		);
	}
}

function getErrorMessage(error) {
	if (error && error.message) {
		return error.message;
	} else {
		return "Sorry an error occured, please try to reload or try again later.";
	}
}