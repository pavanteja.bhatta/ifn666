import React from 'react';
import { TagFaces } from '@material-ui/icons';

class AboutComponent extends React.Component {
	constructor(props) {
		super(props);
		this.state = { ...props };
	}

	render() {
		return (
			<div>
				<h3 className="display-5 left">About the project</h3>
				<br />
				<p className="lead">
					This is a project submitted by Pavan Teja Bhatta (N10239111) as a part of <strong><a href="https://blackboard.qut.edu.au/bbcswebdav/pid-8551062-dt-content-rid-31298682_1/courses/IFN666_20se1/IFN666_Assignment1Specification_2%281%29.pdf" target="_blank" rel="noopener noreferrer">Assignment 1</a></strong> for <strong>IFN666 Web and mobile application development</strong>.
				</p>
				<p className="lead">
					<strong>GitLab link for the project: </strong><a href="https://gitlab.com/pavanteja.bhatta/ifn666/-/tree/master" target="_blank" rel="noopener noreferrer">GitLab</a>
				</p>
				<hr className="my-2" />
				<div className="lead">
					<div className="row">
						<div className="col-sm-6">
							<strong>Lecturer:</strong>
						</div>
						<div className="col-sm-6">
							Dr. Alan Woodley&nbsp;<a target="_blank" rel="noopener noreferrer" href="https://staff.qut.edu.au/staff/a.woodley"><TagFaces color="secondary" /></a>
						</div>
						<div className="col-sm-6">
							<strong>Tutor:</strong>
						</div>
						<div className="col-sm-6">
							Ben Rowe
					</div>
					</div>
				</div>
			</div>
		);
	}
}

export default AboutComponent;