import React from 'react';
import LoadingComponent from "./loading/loading";
// import HomeComponent from "./home/home";
// import AboutComponent from "./about/about";
// import StocksComponent from "./stocks/stocks";
const HomeComponent = React.lazy(() => import ("./home/home"));
const AboutComponent = React.lazy(() => import ("./about/about"));
const StocksComponent = React.lazy(() => import ("./stocks/stocks"));

export { HomeComponent, AboutComponent, StocksComponent, LoadingComponent };