import React from 'react';
import './App.css';
import { Nav, NavItem, NavLink, Navbar, NavbarBrand, NavbarToggler, Collapse, Container, Jumbotron } from 'reactstrap';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  // useRouteMatch,
  // useParams
} from "react-router-dom";

import { HomeComponent, AboutComponent, StocksComponent, LoadingComponent } from "./components";

export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
  }

  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <React.Fragment>
        <Router>
          <div className="App">
            <Navbar color="dark" dark expand="md">
              <NavbarBrand tag={Link} to="/home">
                IFN 666
                </NavbarBrand>
              <NavbarToggler onClick={this.toggle} />
              <Collapse navbar isOpen={this.state.isOpen}>
                <Nav className="ml-auto" navbar>
                  <NavItem>
                    <NavLink tag={Link} to="/home">Home</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink tag={Link} to="/stocks">Stocks</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink tag={Link} to="/about">About</NavLink>
                  </NavItem>
                </Nav>
              </Collapse>
            </Navbar>
            <React.Suspense fallback={<Container className="margin-5"><Jumbotron><LoadingComponent></LoadingComponent></Jumbotron></Container>}>
              <main>
                <Container className="margin-5">
                  <Jumbotron>
                    <Switch>
                      <Route path="/about">
                        <AboutComponent />
                      </Route>
                      <Route path="/stocks">
                        <StocksComponent />
                      </Route>
                      <Route path="/home">
                        <HomeComponent />
                      </Route>
                      <Route path="**" strict>
                        <HomeComponent />
                      </Route>
                    </Switch>
                  </Jumbotron>
                </Container>
              </main>
            </React.Suspense>
          </div>
        </Router >
      </React.Fragment>
    );
  }
}
