import config from "../config";

function getStocks(symbol = undefined) {
	let api = config.apiEndPoint + "/all";
	if (symbol) {
		api += "?symbol=" + symbol;
	}

	return fetch(api)
		.then(res => res.json())
		.then(stocks => 
			stocks.map(stock => ({
				symbol: stock.symbol,
				name: stock.name,
				industry: stock.industry,
			}))
		);
}

export { getStocks };