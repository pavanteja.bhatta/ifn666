import { useState, useEffect } from "react";
import { getHistory } from "../history.service";

function useHistory(symbol = undefined, date = undefined) {
	const [historyLoading, setLoading] = useState(true);
	const [history, setStocks] = useState([]);
	const [historyError, setError] = useState(null);

	useEffect(() => {
		getHistory(symbol, date)
			.then(history => {
				setStocks(history);
				setLoading(false);
			})
			.catch(e => {
				setError(e);
				setLoading(false);
			});
	}, [symbol, date]);

	return {
		historyLoading,
		history,
		historyError
	};
}

export { useHistory };