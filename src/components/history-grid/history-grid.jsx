import React from 'react';
import { AgGridReact } from 'ag-grid-react';
import { Button, Select, MenuItem, Input } from '@material-ui/core';
import { ArrowBack } from '@material-ui/icons';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import "./history-grid.css";
import StocksComponent from "../stocks/stocks";

import * as datefns from "date-fns";
import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';
import {
	MuiPickersUtilsProvider,
	KeyboardDatePicker,
} from '@material-ui/pickers';
import "./history-grid.css";
import { getHistory } from "../../services/history.service";
import { Line } from "react-chartjs-2";


class HistoryGridComponent extends React.Component {

	minDate;
	maxDate;
	searchedData;
	gridApi;

	constructor(props) {
		super(props);
		this.state = {
			selectedDate: new Date('2019-12-12T21:11:54'),
			columnDefs: [{
				headerName: "Date", field: "timestamp", sort: 'asc', sortable: true, valueFormatter: (params) => { return datefns.format(new Date(params.value), "dd/MM/YYYY", { useAdditionalWeekYearTokens: true }) }
			}, {
				headerName: "Symbol", field: "symbol", sortable: true
			}, {
				headerName: "Name", field: "name", sortable: true,
			}, {
				headerName: "Industry", field: "industry", sortable: true,
			}, {
				headerName: "Open", field: "open", sortable: true,
			}, {
				headerName: "High", field: "high", sortable: true,
			}, {
				headerName: "Low", field: "low", sortable: true,
			}, {
				headerName: "Close", field: "close", sortable: true,
			}, {
				headerName: "Volumes", field: "volumes", sortable: true,
			}],
			rowData: this.props.data,
			goBack: false,
			chartData: {},
			chartOptions: {
				fill: false,
				responsive: true,
				scales: {
					xAxes: [{
						type: 'time',
						distribution: 'series',
						display: true,
						ticks: {
							callback: function (tick, index) {
								return (index % 3) ? "" : tick;
							}
						},
						scaleLabel: {
							display: true,
							labelString: "Date",
						}
					}],
					yAxes: [{
						scaleLabel: {
							display: true,
							labelString: "Stock price"
						}
					}]
				}
			},
			chartTypes: ["Closing rate", "Opening rate", "Low", "High"],
			chartType: "Closing rate"
		};
		this.setDates();
	}

	componentDidMount() {
		this.searchedData = this.props.data;
		this.setChartData();
	}

	setChartData(chartType = "Closing rate") {
		let labels = [];
		let data = [];
		this.searchedData.sort(function (a, b) {
			return new Date(a.timestamp) - new Date(b.timestamp);
		});
		this.searchedData.forEach(row => {
			labels.push(new Date(row.timestamp));
			switch (chartType) {
				case "Closing rate":
					data.push(row.close);
					break;
				case "Opening rate":
					data.push(row.open);
					break;
				case "Low":
					data.push(row.low);
					break;
				case "High":
					data.push(row.high);
					break;
				default: 
					data.push(row.close);
			}
		});
		this.setState({
			chartType: chartType,
			chartData: {
				labels: labels,
				datasets: [{
					label: this.searchedData[0].name + " " + chartType,
					backgroundColor: 'rgb(255, 99, 132)',
					borderColor: 'rgb(255, 99, 132)',
					data: data
				}]
			}
		})
	}

	setDates() {
		let dates = [];
		if (this.state.rowData) {
			this.state.rowData.forEach(row => {
				row.timestamp = new Date(row.timestamp);
				dates.push(row.timestamp);
			});
			this.minDate = datefns.min(dates);
			this.maxDate = datefns.max(dates);
		}
	}

	onGridReady = params => {
		this.gridApi = params.api;
		params.api.setRowData(this.searchedData);
	}

	callRefresh(params, gridApi) {
		gridApi.setRowData(params);
		this.setChartData();
	}

	handleDateChange = async (date) => {
		this.setState({
			selectedDate: date
		});
		this.searchedData = await getHistory(this.props.symbol, datefns.format(date, "YYYY-MM-dd", { useAdditionalWeekYearTokens: true }));
		this.callRefresh(this.searchedData, this.gridApi);
	};

	render() {
		if (this.state.goBack) {
			return (
				<div>
					<StocksComponent></StocksComponent>
				</div>
			)
		}
		return (
			<div>
				<br />
				<h4 className="display-5">Stock price table</h4>
				<MuiPickersUtilsProvider utils={DateFnsUtils}>
					<Grid container justify="space-between">
						<div className="vertical-center">
							<Button variant="outlined" className="large" color="secondary" onClick={() => this.clearIndustry()} startIcon={<ArrowBack />}>Go back</Button>
						</div>
						<div className="date-picker">
							<KeyboardDatePicker
								minDate={this.minDate}
								maxDate={this.maxDate}
								margin="normal"
								id="date-picker-dialog"
								label="Pick a date to start"
								format="dd/MM/yyyy"
								value={this.state.selectedDate}
								onChange={this.handleDateChange}
								KeyboardButtonProps={{
									'aria-label': 'change date',
								}}
							/>
						</div>
					</Grid>
				</MuiPickersUtilsProvider>
				<br />
				<div className="ag-theme-alpine flex">
					<AgGridReact
						enableCellChangeFlash={true}
						onGridReady={this.onGridReady}
						columnDefs={this.state.columnDefs}
						pagination="true"
						paginationAutoPageSize="true">
					</AgGridReact>
				</div>
				<br />
				<hr className="my-2" />
				<h4 className="display-5 left">Chart displaying stocks by <Select
					variant="outlined"
					onChange={(event) => this.setChartData(event.target.value)}
					input={<Input value={this.state.chartType} />}>
					{this.state.chartTypes.map((type) => (
						<MenuItem key={type} value={type}>
							{type}
						</MenuItem>
					))}
				</Select></h4>
				<Line height={500} width={700} data={this.state.chartData} options={this.state.chartOptions} />
			</div>
		);
	}

	clearIndustry() {
		this.setState({
			goBack: true
		});
	}
}

export default HistoryGridComponent;