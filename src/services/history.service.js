import config from "../config";

function getHistory(symbol=undefined, date=undefined) {
	let api = config.apiEndPoint + "/history";
	if (symbol !== undefined && date !== undefined) {
		api += "?symbol=" + symbol + "&from=" + date;
	} else if (symbol !== undefined) {
		api += "?symbol=" + symbol;
	} else if (date !== undefined) {
		api += "?from=" + date;
	}

	return fetch(api)
		.then(res => res.json())
		.then(stocks =>
			stocks.map(stock => ({
				timestamp: stock.timestamp,
				symbol: stock.symbol,
				name: stock.name,
				industry: stock.industry,
				open: stock.open,
				high: stock.high,
				low: stock.low,
				close: stock.close,
				volumes: stock.volumes
			}))
		);
}

export { getHistory };