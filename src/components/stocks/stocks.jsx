import React from 'react';
import { useStocks } from '../../services/effects/stocks.effects';
import GridComponent from '../grid/grid';
import LoadingComponent from "../loading/loading";
import { Alert } from "reactstrap";

export default function StocksComponent() {
	var { stocksLoading, stocks, stocksError } = useStocks();
	// const callbackFunction = async (symbol) => {
	// 	stocks = await getIndustry(symbol);
  	// }

	if (stocksLoading) {
		return (
			<LoadingComponent>
			</LoadingComponent>
		);
	} else if (stocksError) {
		return (
			<Alert color="danger">
				{getErrorMessage(stocksError)}
			</Alert>
		);
	} else {
		return (
			// callback={(symbol) => callbackFunction(symbol)}
			<GridComponent data={stocks}></GridComponent>
		);
	}
}

function getErrorMessage(error) {
	if (error && error.message) {
		return error.message;
	} else {
		return "Sorry an error occured, please try to reload or try again later.";
	}
}